#include <string>
#include <ctime>
#include <iomanip>

#include "nota_fiscal.h"
#include "cliente.h"
#include "vendedor.h"
#include "produto.h"

using namespace std;

//Construtor Default
nota_fiscal::nota_fiscal (){

}

//Metodos SET dos atributos da classe
void nota_fiscal::set_item(produto nota_item){

    qt_item[indexItem]=1;
    item[indexItem++] = nota_item;
    if(indexItem > MAX_NOTA)indexItem = 0;
}
void nota_fiscal::inc_qt_item(int index){
    qt_item[index]++;
}
void nota_fiscal::set_nota_vendedor(vendedor vend){

    nota_vendedor = vend;
}

void nota_fiscal::set_nota_cliente(cliente cli){

    nota_cliente = cli;
}


//Métodos GET dos atributos da classe
produto nota_fiscal::get_item(int index){

    return item[index];
}


int nota_fiscal::get_qt_item(int index){
    return qt_item[index];
}

cliente nota_fiscal::get_nota_cliente(){

    return nota_cliente;
}

vendedor nota_fiscal::get_nota_vendedor(){

    return nota_vendedor;
}

//Método que imprime a nota e realiza cálculo dos totalizadores
void nota_fiscal::imprime_nota(){
    //Variáveis 
    float pfinal=0; 
    vendedor vend_nf = get_nota_vendedor();
    cliente clie_nf = get_nota_cliente();
    produto prod_nf;
    int dia,mes,ano,qt;
    //Settings
    time_t now = time(0);
    tm *ltm = localtime(&now);
    dia = 1 + ltm->tm_mon;
    mes = ltm->tm_mday;
    ano = 1900 + ltm->tm_year;
    //itens de teste que faltam importar
    
   
    cout << "====================================" << endl;
    cout << "          Nota Fiscal" << endl;
    cout << "====================================" << endl<< endl;
    
    cout << "Data da emissão: " << dia <<"/"<< mes<<"/"<< ano <<endl;
    cout << "Nome: "<< clie_nf.get_nome() <<endl;
    cout << "Endereço: "<< clie_nf.get_endereco() <<endl;
    cout << "CPF: "<< clie_nf.get_cpf() << endl;
    cout << "===========================================================" << endl;
    cout << "Código\tQt\tDescrição\tP.Unitário\tPreço Total"<< endl;
    cout << "===========================================================" << endl;
    cout.setf(ios::floatfield,ios::fixed);
    for(int i = 0; i< indexItem;i++)
    {
        prod_nf = get_item(i);
        qt = qt_item[i];
        cout << prod_nf.get_codigo()<<"\t"<<qt<<"\t"<<prod_nf.get_descricao();
        cout <<"\t\tR$ "<<setprecision(2)<<prod_nf.get_preco()<<"\t\tR$ ";
        cout <<prod_nf.get_preco()*qt<< endl;
        pfinal += (prod_nf.get_preco()*qt);
    }
    cout << "===========================================================" << endl;
    cout <<"\t\tTOTAL R$\t"<< pfinal << endl;
    cout << "===========================================================" << endl;
    cout <<"Cd do Vendedor: "<< setfill('0') << setw(6) << vend_nf.get_codigo();
    cout <<"\tNome do Vendedor: "<< vend_nf.get_nome() << endl;
    
 
};
