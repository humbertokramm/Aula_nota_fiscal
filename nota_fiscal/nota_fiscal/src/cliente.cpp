#include <string>
#include "cliente.h"


using namespace std;


cliente::cliente ()
{
  nome = "";
  cpf = 0;
  endereco = "";
}

cliente::cliente(string n, unsigned long long c, string e)
{
  nome = n;
  cpf = c;
  endereco = e;
}

string cliente::get_nome()
{
    return nome;
}
unsigned long long cliente::get_cpf()
{
    return cpf;
}
string cliente::get_endereco()
{
    return endereco;
}
void cliente::set_nome(string nome_cliente)
{
  nome = nome_cliente;
}
void cliente::set_cpf(unsigned long long cpf_cliente)
{
  cpf = cpf_cliente;
}
void cliente::set_endereco(string endereco_cliente)
{
    endereco = endereco_cliente;
}