#include <string>
#include "produto.h"

using namespace std;

//Construtor Default
produto::produto(){

    set_codigo(0);
    set_descricao("");
    set_preco(0.0);
}

//Construtor com inicialização de valores
produto::produto(int codigo_produto, string descricao_produto, float preco_produto){

    set_codigo(codigo_produto);
    set_descricao(descricao_produto);
    set_preco(preco_produto);
}


//Metodos SET dos atributos da classe
void produto::set_codigo(int codigo_produto){

    codigo = codigo_produto;
}

void produto::set_descricao(string descricao_produto){

    descricao = descricao_produto;
}

void produto::set_preco(float preco_produto){

    preco = preco_produto;
}

//Metodos GET dos atributos da classe
int produto::get_codigo(){

    return codigo;
}

string produto::get_descricao(){

    return descricao;
}

float produto::get_preco(){

    return preco;
}
