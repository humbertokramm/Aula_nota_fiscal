#include <string>
#include "vendedor.h"


using namespace std;


vendedor::vendedor ()
{
  nome = "";
  codigo = 0;
}

vendedor::vendedor(int c, string n)
{
  nome = n;
  codigo = c;
}

string vendedor::get_nome()
{
    return nome;
}
int vendedor::get_codigo()
{
    return codigo;
}
void vendedor::set_nome(string nome_vendedor)
{
  nome = nome_vendedor;
}
void vendedor::set_codigo(int codigo_vendedor)
{
  codigo = codigo_vendedor;
}
