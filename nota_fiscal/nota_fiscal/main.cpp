/******************************************************************************

Desenvolva o Diagrama de Classes e Implemente um Código Equivalente
Return to: 1 abril - 7 abr...

Objetivo:  Reforçar o desenvolvimento de diagrama de classe e a tradução para o
código.

Implemente um sistema para registrar vendas,  o sistema deverá  emitir uma nota
fiscal composta  pelos produtos vendidos, quantidades de cada produto, preço
unitário de cada produto e preço total.

Ainda é necessário  identificar na nota o cliente, com  nome, CPF e endereço,
assim como o vendedor que  efetuou a venda através do seu nome e código.

Obs.: Este exercício deverá ser desenvolvido em duplas e o diagrama de Classes
apresentado na aula.

*******************************************************************************/


#include <iostream>
#include <string>
#include <sstream>

#define MAX_PRODUTO     5
#define MAX_VENDEDOR    2
#define MAX_CLIENTE     2
#define MAX_NOTA        10
#define MAX_CODE_SYS    255

#include "produto.h"
#include "vendedor.h"
#include "cliente.h"
#include "nota_fiscal.h"


using namespace std;

int main(){

    //Declaração inicial dos vendedores
    vendedor vendedorDB[MAX_VENDEDOR];
    vendedorDB[0] = vendedor(1, "Joaquim");
    vendedorDB[1] = vendedor(5, "Pereira");

    //Declaração inicial dos cliente
    cliente clienteDB[MAX_CLIENTE];
    clienteDB[0] = cliente("Maria", 11111111111, "Av. Teste, 123, Bairro Teste - Cidade Teste");
    clienteDB[1] = cliente("Joao", 22222222222, "Av. Teste, 555, Bairro Teste - Cidade Teste");

    //Declaração inicial dos produtos
    produto produtoDB[MAX_PRODUTO];
    produtoDB[0] = produto(1, "Banana", 2.50);
    produtoDB[1] = produto(2, "Maçã", 1.25);
    produtoDB[2] = produto(3, "Morango", 5.00);
    produtoDB[3] = produto(7, "Pera", 3.50);
    produtoDB[4] = produto(5, "Uva", 7.00);
    
    nota_fiscal nova_nota;


    //Variáveis temporárias
    int temp_cod_vendedor = 0;
    long long temp_cpf_cliente = 0;
    int contador_itens = 0;
    //char temp_cod_item = 0;
    string temp_cod_item;
    int temp_item_qte = 0;
    int insere_itens = 1;
    bool hold = true;
    int index;

    cout << "====================================" << endl;
    cout << "             Nova nota" << endl;
    cout << "====================================" << endl << endl;
    while(hold)
    {
        cout << "Insira o codigo do vendedor: ";
        cin  >> temp_cod_vendedor;
         
        
        //Verifica vendedor pelo código e envia para a nota
        for(index=0; index<MAX_VENDEDOR;index++)
        {
            if(vendedorDB[index].get_codigo() == temp_cod_vendedor) break;
        }
        if(index<MAX_VENDEDOR)
        {
            nova_nota.set_nota_vendedor(vendedorDB[index]);
            cout << "Vendedor: " << vendedorDB[index].get_codigo();
            cout << " - " << vendedorDB[index].get_nome() << endl << endl;
            hold = false;
        }
        else cout << "Vendedor nao encontrado!" << endl << endl;
    }
    hold = true;
    while(hold)
    {
        cout << "Informe o CPF do cliente: ";
        cin  >> temp_cpf_cliente;
    
        //Verifica o cliente pelo cpf e envia para a nota
        for(index=0; index<MAX_CLIENTE;index++)
        {
            if(clienteDB[index].get_cpf() == temp_cpf_cliente) break;
        }
        if(index<MAX_CLIENTE)
        {
            nova_nota.set_nota_cliente(clienteDB[index]);
            cout << "Cliente: " << clienteDB[index].get_nome() << " - ";
            cout << clienteDB[index].get_cpf();
            cout << " - " << clienteDB[index].get_endereco() << endl << endl;
            hold = false;
        }
        else cout << "Cliente nao encontrado!" << endl << endl;
    }
    
    cout << "====================================" << endl;
    cout << "          Insira os itens" << endl;
    cout << "====================================" << endl;


    //Insere itens enquanto não sair ou atigit o numero de itens maximo
    while(insere_itens)
    {
        int codigoProduto;
        cout << endl << "Insira novo item [Tecle E para finalizar nota]: ";
        cin  >> temp_cod_item;
        istringstream(temp_cod_item) >> codigoProduto;
        if(temp_cod_item == "E" || temp_cod_item == "e")
        {
            insere_itens = 0;
            cout << endl;
            cout << "====================================" << endl;
            cout << "        Nota finalizada" << endl;
            cout << "====================================" << endl;
            nova_nota.imprime_nota();
        }  
        else if ( (codigoProduto >= 0) && (codigoProduto < MAX_CODE_SYS) )
        { //produto 1
            int indexed,codigoTemp;
            //Procura na lista de produtos cadastrados
            for(index=0; index<MAX_PRODUTO;index++)
            {
                if(produtoDB[index].get_codigo() == codigoProduto) break;
            }
            if(index<MAX_PRODUTO)
            {
                cout << endl << "Produto: ";
                cout << produtoDB[index].get_codigo() << " - ";
                cout << produtoDB[index].get_descricao() << endl;
                
                codigoTemp = produtoDB[index].get_codigo();
                //Procura na lista de itens cadastrados
                for(indexed = 0 ; indexed<contador_itens ; indexed++)
                {
                    produto prodTemp; 
                    prodTemp = nova_nota.get_item(indexed);
                    if(prodTemp.get_codigo() == codigoTemp) break;
                }
                if(indexed<contador_itens)
                {
                    nova_nota.inc_qt_item(indexed);
                    cout << "Quantidade incrementada"<<endl;
                    cout << "Total = "<< nova_nota.get_qt_item(indexed)<<endl;
                }
                else 
                {
                    nova_nota.set_item(produtoDB[index]);
                    contador_itens ++;
                }
                if(contador_itens > MAX_NOTA)
                {
                    insere_itens = 0;
                    cout << endl;
                    cout << "====================================" << endl;
                    cout << "    Limite de itens atingido" << endl;
                    cout << "        Nota finalizada" << endl;
                    cout << "====================================" << endl;
                }
                
            }
            else cout << "Produto não encontrado!" << endl << endl;
            
            
            
        }
        else cout << endl << "Produto não encontrado!" << endl;
    }
    return 0;
}
