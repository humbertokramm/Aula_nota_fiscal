#ifndef PRODUTO_H
#define PRODUTO_H
#include <string>

using namespace std;

class produto
{
    private:

        int codigo;
        string descricao;
        float preco;


    public:
        produto();
        produto(int codigo_produto, string descricao_produto, float preco_produto);

        void set_codigo(int codigo_produto);
        void set_descricao(string descicao_produto);
        void set_preco(float preco_produto);

        int get_codigo();
        string get_descricao();
        float get_preco();

};

#endif // PRODUTO_H
