#ifndef CLIENTE_H
#define CLIENTE_H
#include <string>

using namespace std;

class cliente
{
    private:
        string nome;
        unsigned long long cpf;
        string endereco;

    public:
        cliente();
        cliente(string n,unsigned long long c,string e);

        string get_nome();
        unsigned long long get_cpf();
        string get_endereco();
        
        void set_nome(string nome_cliente);
        void set_cpf(unsigned long long cpf_cliente);
        void set_endereco(string endereco_cliente);

    protected:
};

#endif // CLIENTE_H