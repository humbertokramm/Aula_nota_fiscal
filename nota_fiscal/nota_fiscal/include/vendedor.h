#ifndef VENDEDOR_H
#define VENDEDOR_H
#include <string>

using namespace std;

class vendedor
{
    private:
        string nome;
        int codigo;

    public:
        vendedor();
        vendedor(int c, string n);

        string get_nome();
        int get_codigo();

        void set_nome(string nome_vendedor);
        void set_codigo(int codigo_vendedor);

    protected:
};

#endif // VENDEDOR_H
