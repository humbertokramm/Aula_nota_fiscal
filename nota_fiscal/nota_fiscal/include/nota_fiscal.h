#ifndef NOTA_FISCAL_H
#define NOTA_FISCAL_H

#include "cliente.h"
#include "vendedor.h"
#include "produto.h"
#include <string>
#include <iostream>

#define MAX_NOTA        10

using namespace std;

class nota_fiscal
{
    private:
        cliente nota_cliente;
        vendedor nota_vendedor;
        produto item[MAX_NOTA];
        //produto item;
        int indexItem=0;
        int qt_item[MAX_NOTA];

    public:
        nota_fiscal();
        void set_item(produto nota_item);
        void inc_qt_item(int index);
        void set_nota_vendedor(vendedor vend);
        void set_nota_cliente(cliente cli);

        produto get_item(int index);
        int get_qt_item(int index);
        vendedor get_nota_vendedor();
        cliente get_nota_cliente();

        void imprime_nota();

    protected:
};

#endif // NOTA_FISCAL_H
